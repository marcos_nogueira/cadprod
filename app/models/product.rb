class Product
  include Mongoid::Document
  include Mongoid::Timestamps
  field :sku, type: String
  field :name, type: String
  field :description, type: String
  field :qty, type: Integer
  field :preco, type: Float
end
